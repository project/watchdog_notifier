<?php

/**
 * @file
 * Schema-generating function(s) for the Watchdog Notifier module.  Executed
 * during install and uninstall of the module.
 */


function watchdog_notifier_generate_entities_table() {
  $watchdog_notifier_array = array(
    'description' => 'The base table for monitoring the watchdog log for specific events.',
    'fields'    => array(
      'wn_id' => array(
        'description' => 'The primary identifier for a watchdog watchlist.',
        'type'        => 'serial',
        'unsigned'    => TRUE,
        'not null'    => TRUE,
      ),
      'email' => array(
        'description' => 'The email address for the recipient of a particular watchdog type/severity.',
        'type'        => 'varchar',
        'length'      => 255,
        'not null'    => TRUE,
        'default'     => '',
      ),
      'type' => array(
        'description' => 'The type of watchdog log message to scan for.',
        'type'        => 'varchar',
        'length'      => 255,
        'not null'    => TRUE,
        'default'     => '',
      ),
      'severity' => array(
        'description' => 'The severity of the type of watchdog log message to scan for.',
        'type'        => 'varchar',
        'length'      => 255,
        'not null'    => TRUE,
        'default'     => '-1',
      ),
      'delivery_method' => array(
        'description' => 'Indicates whether notification delivery should occur immediately, during a regular cron interval, after a threshold is reached, or in digest format.',
        'type'        => 'varchar',
        'length'      => 255,
        'not null'    => TRUE,
        'default'     => 'immediate',
      ),
      'threshold_messages_limit' => array(
        'description' => 'Maximum number of messages allowed before triggering an alert.',
        'type'        => 'int',
        'not null'    => TRUE,
        'unsigned'    => TRUE,
        'default'     => 100,
      ),
      'threshold_time_range' => array(
        'description' => 'Time range (in minutes) in which messages are allowed to accrue to the threshold before triggering an alert.',
        'type'        => 'int',
        'not null'    => TRUE,
        'unsigned'    => TRUE,
        'default'     => 60,
      ),
      'last_run' => array(
        'description' => 'The UNIX timestamp for when this entry was last checked against the watchdog log.',
        'type'        => 'int',
        'not null'    => TRUE,
        'unsigned'    => TRUE,
        'default'     => 0,
      ),
      'bundle_type' => array(
        'description' => 'The bundle type',
        'type'        => 'varchar',
        'length'      => 255,
        'not null'    => TRUE,
        'default'     => 'watchdog_notifier_bundle',
      ),
      'created' => array(
        'description' => 'The UNIX timestamp for when this entry was added to the watchlist.',
        'type'        => 'int',
        'not null'    => TRUE,
        'unsigned'    => TRUE,
        'default'     => 0,
      ),
      'changed' => array(
        'description' => 'The UNIX timestamp of the modification of this entry.',
        'type'        => 'int',
        'not null'    => TRUE,
        'unsigned'    => TRUE,
        'default'     => 0,
      ),
    ),
    'primary key' => array('wn_id'),
  );

  return $watchdog_notifier_array;
}
