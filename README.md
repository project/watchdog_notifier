CONTENTS OF THIS FILE
---------------------
* Introduction
* Requirements
* Installation
* Configuration
* Upgrading
* Permissions
* Maintainers
* Special Thanks


INTRODUCTION
------------
The Watchdog Notifier module allows for the configuration of automatic email
notifications when certain log messages appear in the watchdog log.  Log
messages are matched against type and severity.  Delivery can be made
immediately upon log message generation, scheduled for delivery during specific
times, compiled into periodic digest emails, or triggered when configured
thresholds are exceeded.


REQUIREMENTS
------------
The Job Scheduler module is required.
https://www.drupal.org/project/job_scheduler


INSTALLATION
------------
Install as you would normally install a contributed drupal module. See:
https://drupal.org/documentation/install/modules-themes/modules-7
for further information.


CONFIGURATION
-------------
There are two steps to configuring this module:

1. Configure the watchlist.
2. Configure the delivery timing settings.

Setting up the watchlist is fairly straightforward.  From the Watchdog Notifier
admin screen (/admin/config/system/watchdog_notifier), click "Add a watchdog
notifier."  Fill in the various fields:
 - recipient email address
 - the watchdog log type (php, system, page not found, etc.) to be monitored for
 - the severity to be monitored for (all messages, emergency, alerts, etc.)
 - notification method

There are four types of notification methods:
 - 'Immediately' sends an email for each message immediately upon its appearance
    in the watchdog log.
 - 'Interval' queues up an email for each message for delivery only when cron
    runs.
 - 'Threshold' waits until a preconfigured message per time range limit is
    exceeded before sending an email during the next cron run.
 - 'Digest' groups all messages over a preconfigured period of time into one
    email, which is sent on the next cron run.

If 'Threshold' is selected, a new set of options will appear:
 - 'Max Messages Allowed'
 - 'Time Range (in minutes)'
These two settings dictate the threshold that must be met or exceeded for a
notification email to be sent.

Once the watchlist is configured, proceed to the 'Delivery Settings' tab to set
up cron delivery times.

NOTE: if you are upgrading from Watchdog Notifier 1.x, pre-configured notifiers
will be carried forward as either 'Immediately' or 'Interval' delivery, based on
how scheduling was configured before the upgrade.  If UNIX-style cron was used,
the timing settings are used instead of the 2.x defaults.

Under the 'Delivery Settings' tab, there are three sections for configuration:
 - 'Interval Delivery Settings'
 - 'Threshold Delivery Settings'
 - 'Digest Delivery Settings'
These three sections affect only those watchlist entries that are configured for
that type of delivery.  Watchlist entries set for 'immediate' delivery have no
delivery settings.

Under each of these delivery settings sections, the admin can configure
independent timing rules for each delivery type.  Want 'interval' messages to be
delivered every 15 minutes between 8am and 8pm?  Want 'threshold' messages to be
delivered 24 hours a day during the weekdays?  Want daily digests to be sent
every day at 5:30pm?  All three can be configured independently of each other.

Note that the 'Digest Delivery Settings' section has an extra setting: 'Number
of hours between digest delivery'.  This setting allows more refined control
over periodic digest delivery.  Setting the digest cron config to send half past
the hour between 8am and 6pm means the recipient will receive 10 digest messages
throughout the day.  Setting 'Number of hours between digest delivery' to 2
(hours) means the recipient will instead receive only 5 digest messages.  This
extra setting may seem redundant or superfluous, and this may be true.  It's
been included in this first release of 2.x as an experiment and may go away in
subsequent releases.

Finally, it's important to know that Job Scheduler is used to schedule these
different delivery types.  Job Scheduler relies on Drupal Cron to run often
enough to meet the configured cron settings.  By default, the smallest time
period that Drupal Cron allows the admin to set is 1 hour.  To get more
granularity out of Drupal Cron, it is necessary to set up some external call to
Drupal to execute its Cron functionality.  Refer to this link for more
information:

https://www.drupal.org/docs/7/setting-up-cron-for-drupal/configuring-cron-jobs-using-the-cron-command


UPGRADING
---------
If upgrading from 1.x to 2.x, all settings except the watchlist are
automatically carried over.  To upgrade the watchlist, go to the main Watchdog
Notifier admin screen and click on the "Migrate from 1.x" menu tab.  This menu
provides a count of the number of notifiers in the 1.x watchlist and a button to
import them over to the 2.x format watchlist (basically converting them from
regular database rows over to drupal entities).  Once they've been imported, the
"Migrate from 1.x" menu tab will disappear, and the upgrade is complete.


PERMISSIONS
-----------
If someone other than user 1 needs to configure the watchlist or the cron
settings, set the "Administer the watchlists and frequency of scans for the
watchdog notifier" permission for that user's role.


MAINTAINERS
-----------
Current maintainers:
* Adam Fuller (dasfuller) - https://drupal.org/user/2731951


SPECIAL THANKS
--------------
DamienMcKenna for providing bugfixes and patches that enabled the immediate-send
feature for notifications in the 7.x-1.x branch.  Those changes have been
carried forward into this new version.
