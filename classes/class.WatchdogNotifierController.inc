<?php

/**
 * WatchdogNotifierController extends EntityAPIController.
 *
 * Our subclass of EntityAPIController lets us add a few important create,
 * update, and delete methods.
 */
class WatchdogNotifierController extends EntityAPIController {

  /**
   * Create and return a new watchdog_notifier entity.
   */
  public function create(array $values = array()) {
    return $this->makeNew($values);
  }

  public static function makeNew(array $values = array()) {
    $entity              = new stdClass();
    $entity->wn_id       = 0;
    $entity->bundle_type = 'watchdog_notifier_bundle';
    return $entity;
  }


  /**
   * Saves the custom fields using drupal_write_record().
   */
  public function save($entity, $transaction = NULL) {

    // If our entity has no wn_id, then we need to give it a
    // time of creation.
    if (empty($entity->wn_id)) {
      $entity->created  = time();
      $entity->changed  = $entity->created;
      if (empty($entity->last_run)) {
        $entity->last_run = $entity->created;
      }
    } else {
      $entity->changed = time();
    }

    module_invoke_all('entity_presave', $entity, 'watchdog_notifier');

    // The 'primary_keys' argument determines whether this will be an insert
    // or an update. So if the entity already has an ID, we'll specify
    // wn_id as the key.
    $primary_keys = $entity->wn_id ? 'wn_id' : array();

    // Write out the entity record.
    drupal_write_record('watchdog_notifier', $entity, $primary_keys);

    return $entity;
  }

  /**
   * Delete a single entity.
   *
   * Really a convenience function for deleteMultiple().
   */
  public function delete($entity, $transaction = NULL) {
    $this->deleteMultiple(array($entity));
  }

  /**
   * Delete one or more watchdog_notifier entities.
   *
   * Deletion is unfortunately not supported in the base
   * DrupalDefaultEntityController class.
   *
   * @param array $entities
   *   An array of entity IDs or a single numeric ID.
   */
  public function deleteMultiple($entities) {
    $wn_ids = array();
    if (!empty($entities)) {
      $transaction = db_transaction();
      try {
        foreach ($entities as $entity) {
          // Invoke hook_entity_delete().
          module_invoke_all('entity_delete', $entity, 'watchdog_notifier');
          $wn_ids[] = $entity->wn_id;
        }
        db_delete('watchdog_notifier')
          ->condition('wn_id', $wn_ids, 'IN')
          ->execute();
      }
      catch (Exception $e) {
        $transaction->rollback();
        watchdog_exception('watchdog_notifier', $e);
        throw $e;
      }
    }
  }
}
