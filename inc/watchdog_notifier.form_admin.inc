<?php

/**
 * @file
 * Admin form code for the Watchdog Notifier module.
 */


/**
 * Display list of all configured notifiers.
 * @return mixed
 *   Render array of configured notifiers.
 */
function watchdog_notifier_page() {
  $header = array(
    array('data' => t(''), 'field' => 'wn_id'),
    array('data' => t('Email'), 'field' => 'email'),
    array('data' => t('Log Type'), 'field' => 'type'),
    array('data' => t('Log Severity'), 'field' => 'severity'),
    array('data' => t('Delivery Method'), 'field' => 'delivery_method'),
    array('data' => t('Last Run'), 'field' => 'last_run'),
    array('data' => t('Created'), 'field' => 'created'),
    array('data' => t('Updated'), 'field' => 'changed'),
  );

  // Now do the query for the orders.
  $watchdog_notifier_array = array();
  $query = db_select('watchdog_notifier','wn')
    ->extend('PagerDefault')->limit(25)
    ->extend('TableSort');

  $query->fields('wn', array(
      'wn_id',
      'email',
      'type',
      'severity',
      'delivery_method',
      'last_run',
      'bundle_type',
      'created',
      'changed',
    )
  );

  $result = $query
    ->orderByHeader($header)
    ->execute();

  $rows = array();
  foreach ($result as $res) {
    $rows[] = array('data' => array(
      'wn_id'           => l('Edit', 'admin/config/system/watchdog_notifier/view/' . $res->wn_id),
      'email'           => $res->email,
      'type'            => $res->type,
      'severity'        => watchdog_notifier_get_severity($res->severity),
      'delivery_method' => '<strong>' . ucfirst($res->delivery_method) . '</strong>',
      'last_run'        => date("M j, Y @ H:i:s", $res->last_run),
      'created'         => date("M j, Y", $res->created),
      'changed'         => date("M j, Y", $res->changed),
    ));
  }

  // $build = theme('table');
  $build = theme('table', array(
    'header'  => $header,
    'rows'    => $rows,
    'caption' => t('To view/configure threshold limits, edit the notifier.'),
    'empty'   => t('The watchlist is currently empty.'),
  ));
  $build .= theme('pager');

  return $build;
}


/**
 * Form function to create/edit an watchdog_notifier entity.
 */
function watchdog_notifier_add_notifier_form($form, &$form_state, $entity) {
  $button_text = 'Add Watchdog Notifier';
  $action      = 'add';
  if ($entity->wn_id > 0) {
    $button_text = 'Update Watchdog Notifier';
    $action      = 'update';
  }

  $form['watchlist'] = array(
    '#type'        => 'fieldset',
    '#title'       => t('@title', array('@title' => $button_text)),
    '#description' => t('To @action an entry to the watchlist, enter the
      recipient\'s email address and select the type and severity of the
      watchdog message, then click \'Add to Watchlist.\'', array(
        '@action' => $action,
    )),
    '#collapsible' => FALSE,
    '#collapsed'   => FALSE,
  );

  $form['watchlist']['email'] = array(
    '#type'          => 'textfield',
    '#title'         => t('Email Address'),
    '#description'   => t('Enter the email address of the recipient for this
      watchdog notification'),
    '#required'      => TRUE,
    '#size'          => 30,
    '#maxlength'     => 60,
    '#default_value' => (!empty($entity->email) ? $entity->email : variable_get('site_mail', '')),
  );

  $form['watchlist']['type'] = array(
    '#type'              => 'textfield',
    '#title'             => t('Watchdog Log Type'),
    '#description'       => t('Select the watchdog log type to monitor.'),
    '#required'          => TRUE,
    '#maxlength'         => 128,
    '#autocomplete_path' => 'watchdog_notifier_types/autocomplete',
    '#default_value'     => (!empty($entity->type) ? $entity->type : ''),
  );

  // Set severity level options.
  $severity_array = array(
    '1' => t('Emergency'),
    '2' => t('Alert'),
    '3' => t('Critical'),
    '4' => t('Error'),
    '5' => t('Warning'),
    '6' => t('Notice'),
    '7' => t('Informational'),
    '8' => t('Debug'),
  );
  $form['watchlist']['severity'] = array(
    '#type'          => 'checkboxes',
    '#title'         => t('Watchdog Log Severity'),
    '#description'   => t('Select the watchdog log severity to monitor.'),
    '#options'       => $severity_array,
  );
  // Now set the severity field's default values.
  $preselected_severities = (!empty($entity->severity) ? unserialize($entity->severity) : null);
  if (!empty($preselected_severities)) {
    $form['watchlist']['severity']['#default_value'] = $preselected_severities;
  }

  $form['watchlist']['how_to_notify'] = array(
    '#type'          => 'radios',
    '#title'         => t('Notification Method'),
    '#description'   => t('What sort of notification should be sent?
      <ul>
      <li>\'Immediately\' sends an email for each message immediately upon its
      appearance in the watchdog log.
      <li>\'Interval\' queues up an email for each message for delivery only
      when cron runs.
      <li>\'Threshold\' waits until a preconfigured message per time range limit
      is exceeded before sending an email during the next cron run.
      <li>\'Digest\' groups all messages over a preconfigured period of time
      into one email, which is sent on the next cron run.
      </ul>'),
    '#options'       => array(
      'immediate' => t('Immediately (send notification as messages occur)'),
      'interval'  => t('Interval (queue notification until cron runs)'),
      'threshold' => t('Threshold (wait until number of messages per time range exceeds limit)'),
      'digest'    => t('Digest (group multiple messages into one email)'),
    ),
    '#default_value' => (!empty($entity->delivery_method) ? $entity->delivery_method : 'immediate'),
  );

  $form['watchlist']['threshold'] = array(
    '#type'        => 'fieldset',
    '#title'       => t('Threshold Settings'),
    '#description' => t('If \'Threshold\' is selected above for notifications,
      set the number of messages per time range allowed before a notification is
      sent.  One single notification will be sent each time this threshold is
      exceeded.  For example, if \'Max Messages Allowed\' is set to 100 and
      \'Time Range\' is set to 60, then at least 101 messages within a rolling
      60 minute period will need to appear in the watchdog log before a
      notification is sent.'),
    '#collapsible' => TRUE,
    '#collapsed'   => FALSE,
    '#states' => array(
      'visible' => array(
        ':input[name="how_to_notify"]' => array('value' => 'threshold'),
      ),
    ),
  );
  $form['watchlist']['threshold']['max_messages_allowed'] = array(
    '#type'          => 'textfield',
    '#title'         => t('Max Messages Allowed'),
    '#description'   => t('Maximum number of messages allowed during the time
      range before a single notification is fired.'),
    '#required'      => TRUE,
    '#size'          => 10,
    '#maxlength'     => 10,
    '#default_value' => (!empty($entity->threshold_messages_limit) ? $entity->threshold_messages_limit : 100),
  );
  $form['watchlist']['threshold']['time_range'] = array(
    '#type'          => 'textfield',
    '#title'         => t('Time Range (in minutes)'),
    '#description'   => t('Time range (in minutes) over which a max number of
      messages is allowed to accumulate before a notification is sent.'),
    '#required'      => TRUE,
    '#size'          => 10,
    '#maxlength'     => 10,
    '#default_value' => (!empty($entity->threshold_time_range) ? $entity->threshold_time_range : 60),
  );

  // ---------------------------------------------------------------------------
  // Needed for the entity biz.
  $form['watchlist']['watchdog_notifier_entity'] = array(
    '#type'  => 'value',
    '#value' => $entity,
  );

  $form['watchlist']['submit'] = array(
    '#type'     => 'submit',
    '#value'    => $button_text,
    '#validate' => array('watchdog_notifier_add_notifier_form_validate'),
    '#submit'   => array('watchdog_notifier_add_notifier_form_submit'),
  );
  $form['watchlist']['cancel'] = array(
    '#type'                    => 'submit',
    '#value'                   => t('Cancel'),
    '#limit_validation_errors' => array(),
    '#submit'                  => array('watchdog_notifier_cancel_button'),
    '#weight'                  => 200,
  );
  if (0 != $entity->wn_id) {
    $form['watchlist']['delete'] = array(
      '#type'   => 'submit',
      '#value'  => t('Delete Notifier'),
      '#submit' => array('watchdog_notifier_do_delete'),
      '#weight' => 300,
    );
  }

  return $form;
}


/**
 * Form builder.  Validate Watchdog Notifier configuration.
 */
function watchdog_notifier_add_notifier_form_validate($form, &$form_state) {
  // Validate email.
  if (!filter_var($form_state['values']['email'], FILTER_VALIDATE_EMAIL)) {
    form_set_error('email', 'Invalid email address.');
  }

  // Don't worry about validating the type.  Users should be able to enter
  // whatever their heart's desire.  The form will automatically catch any blank
  // entries.

  // Validate the watchdog log severity.
  foreach ($form_state['values']['severity'] as $key => $value) {
    if ((is_string($value)) && ($key != intval($value))) {
      form_set_error('severity', 'Invalid watchdog log severity.');
    } elseif ((is_int($value)) && ($value != 0)) {
      form_set_error('severity', 'Invalid watchdog log severity.');
    }
  }

  // Validate how to notify.
  $verify_how_to_notify = 0;
  foreach (array('immediate','interval','threshold','digest') as $key => $value) {
    if ($form_state['values']['how_to_notify'] == $value) {
      $verify_how_to_notify++;
    }
  }
  if ($verify_how_to_notify != 1) {
    form_set_error('how_to_notify', 'Invalid \'how to notify\' setting.');
  }

  // If threshold is selected, validate the message/time thresholds.
  if ($form_state['values']['how_to_notify'] == 'Threshold') {
    if (!filter_var($form_state['values']['max_messages_allowed'], FILTER_VALIDATE_INT)) {
      form_set_error('max_messages_allowed', 'Invalid max messages allowed threshold.');
    }
    if (!filter_var($form_state['values']['time_range'], FILTER_VALIDATE_INT)) {
      form_set_error('time_range', 'Invalid threshold time range.');
    }
  }
}


/**
 * Form submit handler: Submits watchdog_notifier_form information.
 */
function watchdog_notifier_add_notifier_form_submit($form, &$form_state) {
  $entity = $form_state['values']['watchdog_notifier_entity'];

  $entity->email                    = $form_state['values']['email'];
  $entity->type                     = check_plain($form_state['values']['type']);
  $entity->severity                 = serialize($form_state['values']['severity']);
  $entity->delivery_method          = check_plain($form_state['values']['how_to_notify']);
  $entity->threshold_messages_limit = $form_state['values']['max_messages_allowed'];
  $entity->threshold_time_range     = $form_state['values']['time_range'];
  $entity->last_run                 = time();
  $entity = watchdog_notifier_save($entity);

  watchdog(
    'watchdog_notifier',
    'Notifier has been added/updated: <pre>@config</pre>',
    array(
      '@config'  => print_r($entity, TRUE),
    ),
    WATCHDOG_INFO
  );

  $form_state['redirect'] = 'admin/config/system/watchdog_notifier';
}


/**
 * Form function to configure notification timing.
 */
function watchdog_notifier_timing_form($form, &$form_state) {
  $cron_last           = variable_get('cron_last');
  $cron_safe_threshold = variable_get('cron_safe_threshold', '-1');
  $cron_timing = array(
    '-1'     => 'Unknown',
    '0'      => 'Never',
    '3600'   => '1 hour',
    '10800'  => '3 hours',
    '21600'  => '6 hours',
    '43200'  => '12 hours',
    '86400'  => '1 day',
    '604800' => '1 week',
  );

  $form['the_whole_shebang'] = array(
    '#type'        => 'fieldset',
    '#title'       => t('Site-wide Delivery Settings'),
    '#description' => t('These settings are site-wide.  While notifiers
      configured for \'Immediate\' delivery fire off emails when a matching log
      entry is generated, the remaining delivery types require Job Scheduler to
      be enabled and running.  They match against watchdog log entries that have
      been created since the last time that delivery type ran.'),
    '#collapsible' => FALSE,
    '#collapsed'   => FALSE,
  );

  $form['the_whole_shebang']['cron_settings'] = array(
    '#type'        => 'item',
    // '#title'       => t('Current Cron Settings'),
    '#description' => t('<p>Job Scheduler is dependent on Drupal Cron, which is
      currently configured to run every %timing. ' .
      l('Click here', 'admin/config/system/cron') . ' to change Drupal Cron
      settings.</p>
      <p>Cron last ran %cron_last ago.</p>',
      array(
        '%timing'    => $cron_timing[$cron_safe_threshold],
        '%cron_last' => format_interval(REQUEST_TIME - $cron_last),
      )
    ),
  );

  // $form['the_whole_shebang']['immediate_settings'] = array(
  //   '#type'        => 'fieldset',
  //   '#title'       => t('Immediate Delivery Settings'),
  //   // '#description' => t('Immediate delivery cannot be restricted to any specific time period.'),
  //   '#collapsible' => FALSE,
  //   '#collapsed'   => FALSE,
  // );

  foreach (array('interval','threshold','digest') as $key => $type) {
    $form['the_whole_shebang'][$type . '_settings'] = array(
      '#type'        => 'fieldset',
      '#title'       => t(ucfirst($type) . ' Delivery Settings'),
      // '#description' => t('Configure the Unix cron schedule for delivery of notifications set to \'' . ucfirst($type) . '\'.'),
      '#collapsible' => FALSE,
      '#collapsed'   => FALSE,
    );

    if ($type == 'digest') {
      $generate_digest = variable_get('watchdog_notifier_generate_digest');
      $timing_string = '';
      if ($generate_digest >= 1) {
        $timing_string = $generate_digest . ' hour(s)';
      } else {
        $timing_string = ($generate_digest * 60) . ' minute(s)';
      }

      $form['the_whole_shebang'][$type . '_settings'][$type . '_generate'] = array(
        '#type'          => 'textfield',
        '#title'         => t('Number of hours between digest delivery'),
        '#description'   => t('The cron configuration below dictates the
          delivery window for digests to be sent. %this setting compliments that
          by determining how often within that window digests are sent out.
          This setting tells Watchdog Notifier how many hours to wait between
          checks to see if the cron window for digest delivery is open.  If it
          is open, then the digest is sent.  Otherwise Watchdog Notifier waits
          until the next scheduled check.',
          array('%this' => 'This')),
        '#size'          => 10,
        '#maxlength'     => 10,
        '#default_value' => $generate_digest,
      );
    }

    $cron_time = explode(" ", variable_get('watchdog_notifier_cron_time_' . $type));

    $form['the_whole_shebang'][$type . '_settings']['cron'] = array(
      '#type'        => 'fieldset',
      '#title'       => t(ucfirst($type) . ' Cron Configuration'),
      '#description' => t('Configure the Unix cron schedule for delivery of notifications set to \'' . ucfirst($type) . '\'.'),
      '#collapsible' => TRUE,
      '#collapsed'   => TRUE,
    );

    $form['the_whole_shebang'][$type . '_settings']['cron'][$type . '_min'] = array(
        '#type'          => 'textfield',
        '#title'         => t('Minutes'),
      '#description'   => t('Enter a specific minute between 0 and 60 or comma
        separate multiple minutes (e.g. "0,15,30,45" for every 15 minutes).'),
      '#size'          => 20,
      '#maxlength'     => 20,
      '#default_value' => $cron_time[0],
    );

    $form['the_whole_shebang'][$type . '_settings']['cron'][$type . '_hour'] = array(
      '#type'          => 'textfield',
      '#title'         => t('Hour'),
      '#description'   => t('Enter a specific hour between 0 and 23, "*" for
        every hour, or comma separate multiple hours (e.g. "0,6,12,18" for every
        6 hours).'),
      '#size'          => 20,
      '#maxlength'     => 20,
      '#default_value' => $cron_time[1],
    );

    $form['the_whole_shebang'][$type . '_settings']['cron'][$type . '_day_month'] = array(
      '#type'          => 'textfield',
      '#title'         => t('Day of the Month'),
      '#description'   => t('Enter a specific day between 1 and 31, "*" for
        every day of the month, or comma separate multiple days of the month
        (e.g. "1,10,20" for the 1st, 10th, and 20th day of the month).'),
      '#size'          => 20,
      '#maxlength'     => 20,
      '#default_value' => $cron_time[2],
    );

    $form['the_whole_shebang'][$type . '_settings']['cron'][$type . '_month'] = array(
      '#type'          => 'textfield',
      '#title'         => t('Month'),
      '#description'   => t('Enter a specific month between 1 and 12, "*" for
        every month, or comma separate multiple months (e.g. "1,6,11" for
        January, June, and November).'),
      '#size'          => 20,
      '#maxlength'     => 20,
      '#default_value' => $cron_time[3],
    );

    $form['the_whole_shebang'][$type . '_settings']['cron'][$type . '_day_week'] = array(
      '#type'          => 'textfield',
      '#title'         => t('Day of the week'),
      '#description'   => t('Enter a specific day of the week between 0 and 6,
        "*" for every day of the week, or comma/dash separate multiple days
        (e.g. "1-5" for Monday through Friday).'),
      '#size'          => 20,
      '#maxlength'     => 20,
      '#default_value' => $cron_time[4],
    );
  }

  $form['the_whole_shebang']['submit'] = array(
    '#type'     => 'submit',
    '#value'    => t('Save Delivery Settings'),
    '#validate' => array('watchdog_notifier_timing_form_validate'),
    '#submit'   => array('watchdog_notifier_timing_form_submit'),
  );
  $form['the_whole_shebang']['cancel'] = array(
    '#type'                    => 'submit',
    '#value'                   => t('Cancel'),
    '#limit_validation_errors' => array(),
    '#submit'                  => array('watchdog_notifier_cancel_button'),
    '#weight'                  => 300,
  );

  return $form;
}


/**
 * Form builder.  Validate Watchdog Notifier configuration.
 */
function watchdog_notifier_timing_form_validate($form, &$form_state) {
  if (!filter_var($form_state['values']['digest_generate'], FILTER_VALIDATE_FLOAT)) {
    form_set_error('digest_hour', 'Invalid number of hours.');
  }

  $asterisk = preg_quote('*');
  foreach (array('interval','threshold','digest') as $key => $type) {
    $cron_times = array(
      'min'       => $form_state['values'][$type . '_min'],
      'hour'      => $form_state['values'][$type . '_hour'],
      'day_month' => $form_state['values'][$type . '_day_month'],
      'month'     => $form_state['values'][$type . '_month'],
      'day_week'  => $form_state['values'][$type . '_day_week'],
    );
    foreach ($cron_times as $key => $value) {
      if (preg_match("/$asterisk/", $value)) {
        // Field has an asterisk. Everything's cool.
      } else {
        // 1. Explode on commas
        // 2. Cycle through each element, exploding on dashes.
        // 3. Cycle through each subelement, convert string to number.
        // 4. Make sure number is between a certain value for each field.

        $comma_pieces = explode(',', $value);
        foreach ($comma_pieces as $comma_piece) {
          $dash_pieces = explode('-', $comma_piece);
          foreach ($dash_pieces as $dash_piece) {
            $num = (int) $dash_piece;

            if ('min' === $key && $num > 60) {
              form_set_error($key, 'Invalid value for minutes.');
            }
            if ('hour' === $key && $num > 23) {
              form_set_error($key, 'Invalid value for hours.');
            }
            if ('day_month' === $key && (0 == $num || $num > 31)) {
              form_set_error($key, 'Invalid value for days of the month.');
            }
            if ('month' === $key && (0 == $num || $num > 12)) {
              form_set_error($key, 'Invalid value for months.');
            }
            if ('day_week' === $key && $num > 6) {
              form_set_error($key, 'Invalid value for days of the week.');
            }
          }
        }
      }
    }
  }
}


/**
 * Form submit handler: Submits watchdog_notifier_form information.
 */
function watchdog_notifier_timing_form_submit($form, &$form_state) {
  $config = array();

  $config['digest_generate'] = $form_state['values']['digest_generate'];
  variable_set('watchdog_notifier_generate_digest', $form_state['values']['digest_generate']);

  $new_crontab = array();
  foreach (array('interval','threshold','digest') as $key => $type) {
    $crontab = $form_state['values'][$type . '_min'] . ' ' .
      $form_state['values'][$type . '_hour'] . ' ' .
      $form_state['values'][$type . '_day_month'] . ' ' .
      $form_state['values'][$type . '_month'] . ' ' .
      $form_state['values'][$type . '_day_week'];
    variable_set('watchdog_notifier_cron_time_' . $type, $crontab);
    $config[$type . '_new_crontab'] = $crontab;

    $job = array(
      'type'     => 'watchdog_notifier',
      'id'       => variable_get('watchdog_notifier_job_id_' . $type),
      'periodic' => TRUE,
      'crontab'  => $crontab,
    );
    JobScheduler::get('watchdog_notifier_dispatch_' . $type)->remove($job);
    JobScheduler::get('watchdog_notifier_dispatch_' . $type)->set($job);
  }

  drupal_set_message(t('The delivery settings have been saved.'));

  watchdog(
    'watchdog_notifier',
    'Delivery settings have been updated: <pre>@config</pre>',
    array(
      '@config'  => print_r($config, TRUE),
    ),
    WATCHDOG_INFO
  );
}


/**
 * Form function to bring over 1.x config.
 */
function watchdog_notifier_migrate_form($form, &$form_state) {
  $form['migrate'] = array(
    '#type'        => 'fieldset',
    '#title'       => t('Migrate Notifiers From 1.x to 2.x'),
    '#collapsible' => FALSE,
    '#collapsed'   => FALSE,
  );

  $result = db_select('watchdog_notifier_old', 'wn_old')
    ->fields('wn_old')
    ->execute();
  $num_of_rows = $result->rowCount();

  $form['migrate']['description'] = array(
    '#type'          => 'item',
    '#description'   => t('There are currently @num prior configured
      notifier(s). Click \'Migrate Notifiers\' to migrate all old notifiers to
      the new database format.', array('@num' => $num_of_rows)),
  );

  $form['migrate']['submit'] = array(
    '#type'     => 'submit',
    '#value'    => t('Migrate Notifiers'),
    '#validate' => array('watchdog_notifier_migrate_form_validate'),
    '#submit'   => array('watchdog_notifier_migrate_form_submit'),
  );
  $form['migrate']['cancel'] = array(
    '#type'                    => 'submit',
    '#value'                   => t('Cancel'),
    '#limit_validation_errors' => array(),
    '#submit'                  => array('watchdog_notifier_cancel_button'),
    '#weight'                  => 300,
  );

  return $form;
}


/**
 * Form builder.  Validate Watchdog Notifier configuration.
 */
function watchdog_notifier_migrate_form_validate($form, &$form_state) {
  if ($form_state['clicked_button']['#value'] != 'Migrate Notifiers') {
    form_set_error('submit', 'Invalid button press.');
  }
}


/**
 * Form submit handler: Submits watchdog_notifier_form information.
 */
function watchdog_notifier_migrate_form_submit($form, &$form_state) {
  $migrated_watchlist = array();

  // Grab old cron/last_run variables and use those to build new notifier
  // entities.
  $watchdog_notifier_use_cron = variable_get('watchdog_notifier_use_cron');
  $watchdog_notifier_last_run = variable_get('watchdog_notifier_last_run');

  // Update any pre-existing entries.
  $delivery_method = 'immediate';
  if (($watchdog_notifier_use_cron == 'run_with_cron') || ($watchdog_notifier_use_cron == 'run_with_scheduler')) {
    $delivery_method = 'interval';
  }

  $query = db_select('watchdog_notifier_old', 'wn_old')
    ->fields('wn_old');
  $result = $query->execute();
  foreach ($result as $res) {
    $migrated_watchlist[] = $res;

    $entity = WatchdogNotifierController::makeNew();
    $entity->email           = $res->email;
    $entity->type            = $res->type;
    $entity->severity        = $res->severity;
    $entity->delivery_method = $delivery_method;
    $entity->last_run        = $watchdog_notifier_last_run;
    $entity = watchdog_notifier_save($entity);
  }

  db_drop_table('watchdog_notifier_old');

  variable_del('watchdog_notifier_use_cron');
  variable_del('watchdog_notifier_last_run');

  drupal_flush_all_caches();

  drupal_set_message(t('Old notifier migration is complete.'));

  watchdog(
    'watchdog_notifier',
    'Old watchlist has been migrated to new format: <pre>@config</pre>',
    array(
      '@config'  => print_r($migrated_watchlist, TRUE),
    ),
    WATCHDOG_INFO
  );

  drupal_goto('admin/config/system/watchdog_notifier');
}
