<?php

/**
 * @file
 * General code for the Watchdog Notifier module.
 */


/**
 * Provides a wrapper on the list page to add a new entity.
 */
function watchdog_notifier_add() {
  // Create a basic entity structure to be used and passed to the validation
  // and submission functions.
  $entity = entity_get_controller('watchdog_notifier')->create();
  return drupal_get_form('watchdog_notifier_add_notifier_form', $entity);
}


/**
 * Cancel button for add/edit form
 */
function watchdog_notifier_cancel_button($form, &$form_state) {
  drupal_goto('admin/config/system/watchdog_notifier');
}


/**
 * Form deletion handler.
 */
function watchdog_notifier_do_delete($form, &$form_state) {
  $entity = $form_state['values']['watchdog_notifier_entity'];
  watchdog_notifier_delete($entity);
  drupal_set_message(t('The watchdog notifier @wn_id (@type (@severity) going to @email) has been deleted', array(
    '@wn_id'    => $entity->wn_id,
    '@type'     => $entity->type,
    '@severity' => watchdog_notifier_get_severity($entity->severity),
    '@email'    => $entity->email,
  )));
  $form_state['redirect'] = 'admin/config/system/watchdog_notifier';

  watchdog(
    'watchdog_notifier',
    'Notifier has been deleted: <pre>@config</pre>',
    array(
      '@config'  => print_r($entity, TRUE),
    ),
    WATCHDOG_INFO
  );
}


/**
 * Quick function to return the string equivalent of each of numerical severity
 * level.
 */
function watchdog_notifier_get_severity($notification_level = null) {
  $severity_array = array(
    1 => 'Emergency',
    2 => 'Alert',
    3 => 'Critical',
    4 => 'Error',
    5 => 'Warning',
    6 => 'Notice',
    7 => 'Informational',
    8 => 'Debug',
  );

  $severity_string = '';

  if (!empty($severity_array[$notification_level])) {
    $severity_string .= $severity_array[$notification_level];
  } else {
    $given_severities = unserialize($notification_level);

    if ($notification_level == null) {
      $severity_string = 'No severity level has been provided.';
    } else {
      // How many commas?
      $commas_needed = -1;
      foreach ($given_severities as $key => $value) {
        if ((is_string($value)) && ($key == intval($value))) {
          $commas_needed++;
        }
      }

      // Okay, now put it all together.
      foreach ($given_severities as $key => $value) {
        if ((is_string($value)) && ($key == intval($value))) {
          $severity_string .= $severity_array[$value];
          if ($commas_needed > 0) {
            $severity_string .= ', ';
            $commas_needed--;
          }
        }
      }
    }
  }

  return $severity_string;
}


/**
 * Sometimes the job_schedule table gets wonky and 'loses' one of the schedulers
 * created at this module's install.  Pressing the 'Rebuild Job Scheduler table'
 * button on the main watchdog notifier admin page will execute this function to
 * delete old entries and then recreate them with current configured settings.
 */
function watchdog_notifier_rebuild_job_schedule_table() {
  // First delete any pre-existing entries in the job_schedule table.
  foreach (array('interval','threshold','digest') as $key => $type) {
    $job = array(
      'type'     => 'watchdog_notifier',
      'id'       => variable_get('watchdog_notifier_job_id_' . $type),
      'periodic' => TRUE,
      'crontab'  => variable_get('watchdog_notifier_cron_time_' . $type),
    );
    JobScheduler::get('watchdog_notifier_dispatch_' . $type)->remove($job);
  }

  // Now recreate the job_schedule table entries.
  $job_id = 271828;
  foreach (array('interval','threshold','digest') as $key => $type) {
    variable_set('watchdog_notifier_job_id_' . $type, $job_id);

    $job = array(
      'type'     => 'watchdog_notifier',
      'id'       => $job_id,
      'periodic' => TRUE,
      'crontab'  => variable_get('watchdog_notifier_cron_time_' . $type),
    );
    JobScheduler::get('watchdog_notifier_dispatch_' . $type)->set($job);
    $job_id++;
  }

  drupal_set_message(t('Job Scheduler table rebuilt'));
  drupal_goto('admin/config/system/watchdog_notifier');
}

/**
 * Provides a callback/wrapper for the watchdog_notifier_dispatch_digest()
 * function so that digests can be run whenever the admin wants to trigger one.
 */
function watchdog_notifier_run_digest() {
  $num_digests_sent = watchdog_notifier_dispatch_digest(TRUE);
  $extra_msg = '';
  if ($num_digests_sent == 0) {
    $extra_msg = 'If no watchdog log messages match a digest notifier, no email
      for that notifier is sent and it\'s \'Last Run\' field will not update.';
  }
  drupal_set_message(t('@num digest email(s) sent. @extra',
    array(
      '@num'   => $num_digests_sent,
      '@extra' => $extra_msg,
    )
  ));
  drupal_goto('admin/config/system/watchdog_notifier');
}


/**
 * Callback for a page title when this entity is displayed.
 */
function watchdog_notifier_title($entity) {
  return t('@type (@severity) going to @email', array(
    '@type'     => $entity->type,
    '@severity' => watchdog_notifier_get_severity($entity->severity),
    '@email'    => $entity->email,
  ));
}


/**
 * autocomplete helper
 * $string = string for search
 */
function watchdog_notifier_types_autocomplete($string = NULL) {
  // Gather up all the watchdog log types.
  $type_array = array();
  $query = db_select('watchdog','w')
    ->fields('w', array('type'))
    ->distinct()
    ->orderBy('type', 'ASC');

  if (!empty($string)) {
    $query->condition('type', '%' . db_like($string) . '%', 'LIKE');
  }

  $result = $query->execute();
  foreach ($result as $res) {
    $type_array[check_plain($res->type)] = check_plain($res->type);
  }

  if (!empty($string)) {
    drupal_json_output($type_array);
  } else {
    return $type_array;
  }
}


/**
 * Implements the uri callback.
 */
function watchdog_notifier_uri($entity) {
  return array(
    'path' => 'admin/config/system/watchdog_notifier/view/' . $entity->wn_id,
  );
}

