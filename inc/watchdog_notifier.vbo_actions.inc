<?php

/**
 * @file
 * Custom VBO actions for the Watchdog Notifier module.
 */


function watchdog_notifier_vbo_delete(&$entity, $context = array()) {
  return watchdog_notifier_delete($entity);
}
