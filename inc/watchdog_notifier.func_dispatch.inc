<?php
/**
 * @file
 * Dispatch functions for the Watchdog Notifier module.
 *
 * Each delivery method (immediate, threshold, dispatch) has its own Job
 * Scheduler job that does the job of acting on the database entries and
 * watchdog log messages.
 */


/**
 * Fires off emails immediately upon generation of a watchlist-matching watchdog
 * log entry.
 * @param  array  $log_entry
 *   Newly-generated watchdog log message.
 * @return integer
 *   Returns 1 if a message is sent, 0 otherwise.
 */
function watchdog_notifier_dispatch_immediate($log_entry = array()) {
  if (empty($log_entry)) {
    return 0;
  }

  $watchlist = watchdog_notifier_load_multiple(watchdog_notifier_return_watchlist('immediate'));
  if (empty($watchlist)) {
    return 0;
  }

  foreach ($watchlist as $key => $notifier) {
    $severities = unserialize($notifier->severity);
    foreach ($severities as $subkey => $severity) {
      if ((is_string($severity)) && ($severity != '0')) {
        $severity = $severity - 1;
        if (($notifier->type == $log_entry['type']) && ($severity == $log_entry['severity'])) {
          watchdog_notifier_send_email($notifier, $log_entry);
          $notifier->last_run = time();
          $notifier           = watchdog_notifier_save($notifier);
        }
      }
    }
  }

  return 1;
}


/**
 * Fires off emails during cron runs for each new watchlist-matching watchdog
 * log entry.
 * @return integer
 *   Number of emails fired by this function.
 */
function watchdog_notifier_dispatch_interval() {
  $num_emails_sent = 0;
  $watchlist       = watchdog_notifier_load_multiple(watchdog_notifier_return_watchlist('interval'));
  if (empty($watchlist)) {
    return $num_emails_sent;
  }

  // Gather watchdog messages that match criteria in the watchlist.
  $num_emails_sent = 0;

  foreach ($watchlist as $key => $notifier) {
    // Go through this for each severity level configured on the notifier.
    $severities = unserialize($notifier->severity);

    foreach ($severities as $subkey => $severity) {
      if ((is_string($severity)) && ($subkey == intval($severity))) {
        $query = db_select('watchdog', 'w')
          ->fields('w')
          ->condition('type', $notifier->type, '=')
          ->condition('timestamp', $notifier->last_run, '>=')
          ->condition('severity', $severity - 1, '=');

        $result = $query->orderBy('timestamp', 'ASC');
        $result = $query->execute();
        foreach ($result as $res) {
          watchdog_notifier_send_email($notifier, $res);
          $num_emails_sent++;
        }
      }
    }

    $notifier->last_run = time();
    $notifier           = watchdog_notifier_save($notifier);
  }

  watchdog(
    'watchdog_notifier',
    'Interval dispatch has just sent %num message(s).',
    array(
      '%num'  => print_r($num_emails_sent, TRUE),
    ),
    WATCHDOG_INFO
  );

  return $num_emails_sent;
}


/**
 * Waits until a digest timer has passed and then fires off a single email that
 * consolidates all the messages that have come in for watchlist notifiers.
 * @return integer
 *   Number of emails fired by this function.
 */
function watchdog_notifier_dispatch_digest($ignore_digest_generate_timer = FALSE) {
  $generate_digest = variable_get('watchdog_notifier_generate_digest');
  if ((!is_array($ignore_digest_generate_timer)) && ($ignore_digest_generate_timer === TRUE)) {
    $generate_digest = 0;
  }

  $num_emails_sent = 0;

  $watchlist = watchdog_notifier_load_multiple(watchdog_notifier_return_watchlist('digest'));
  if (empty($watchlist)) {
    return $num_emails_sent;
  }

  // Gather watchdog messages that match criteria in the watchlist.
  foreach ($watchlist as $key => $notifier) {
    $watchdog_logs = array();

    $next_digest_due = $notifier->last_run + ($generate_digest * 3600);
    if (time() < $next_digest_due) {
      continue;
    }

    // Go through this for each severity level configured on the notifier.
    $severities = unserialize($notifier->severity);
    foreach ($severities as $subkey => $severity) {
      if ((is_string($severity)) && ($subkey == intval($severity))) {
        $query = db_select('watchdog', 'w')
          ->fields('w')
          ->condition('type', $notifier->type, '=')
          ->condition('timestamp', $notifier->last_run, '>=')
          ->condition('severity', $severity - 1, '=');

        $result = $query->orderBy('timestamp', 'ASC');
        $result = $query->execute();
        foreach ($result as $res) {
          $watchdog_logs[] = $res;
        }
      }
    }

    watchdog_notifier_send_email($notifier, $watchdog_logs);
    $num_emails_sent++;

    $notifier->last_run = time();
    $notifier           = watchdog_notifier_save($notifier);
  }

  watchdog(
    'watchdog_notifier',
    'Digest dispatch has just sent %num message(s).',
    array(
      '%num'  => print_r($num_emails_sent, TRUE),
    ),
    WATCHDOG_INFO
  );

  return $num_emails_sent;
}


/**
 * Counts the number of messages in a rolling time-frame, and, if it matches the
 * configured threshold, fires off an email about the messages.
 * @return integer
 *   Number of emails fired by this function.
 */
function watchdog_notifier_dispatch_threshold() {
  $num_emails_sent = 0;
  $cron_last       = variable_get('cron_last');
  $watchlist       = watchdog_notifier_load_multiple(watchdog_notifier_return_watchlist('threshold'));

  if (empty($watchlist)) {
    return $num_emails_sent;
  }

  $master_time = time();
  foreach ($watchlist as $key => $notifier) {
    $threshold_start_time = $notifier->last_run;
    $threshold_end_time = $threshold_start_time + ($notifier->threshold_time_range * 60);

    while ($threshold_start_time < $master_time) {
      $query = db_select('watchdog', 'w')
        ->fields('w')
        ->condition('type', $notifier->type, '=')
        ->condition('timestamp', $threshold_start_time, '>')
        ->condition('timestamp', $threshold_end_time, '<=');

      // Go through this for each severity level configured on the notifier.
      $db_or = db_or();
      $severities = unserialize($notifier->severity);
      foreach ($severities as $subkey => $severity) {
        if ((is_string($severity)) && ($subkey == intval($severity))) {
          $db_or->condition('severity', $severity - 1, '=');
        }
      }
      $query->condition($db_or);

      $result = $query->orderBy('timestamp', 'ASC');
      $result = $query->execute()->fetchAll();

      if (count($result) == 0) {
        $threshold_start_time = $threshold_end_time;
        $threshold_end_time   = $threshold_end_time + ($notifier->threshold_time_range * 60);

        if ($threshold_start_time < $master_time) {
          $notifier->last_run = $threshold_end_time;
        } else {
          $notifier->last_run = $master_time;
        }

      } elseif (count($result) < $notifier->threshold_messages_limit) {
        $threshold_start_time = $result[0]->timestamp;
        $threshold_end_time   = $threshold_start_time + ($notifier->threshold_time_range * 60);

        $notifier->last_run   = $threshold_start_time;

      } elseif (count($result) >= $notifier->threshold_messages_limit) {
        $threshold_start_time = $threshold_end_time;
        $threshold_end_time   = $threshold_end_time + ($notifier->threshold_time_range * 60);

        $notifier->last_run   = $result[count($result)-1]->timestamp;

        watchdog_notifier_send_email($notifier, $result);
        $num_emails_sent++;
      }
    }

    $notifier = watchdog_notifier_save($notifier);
  }

  watchdog(
    'watchdog_notifier',
    'Threshold dispatch has just sent %num message(s).',
    array(
      '%num'  => print_r($num_emails_sent, TRUE),
    ),
    WATCHDOG_INFO
  );

  return $num_emails_sent;
}


/**
 * Performs an entityfieldquery for a list of all the watchlist entries that
 * match the given delivery type ('immediate', 'interval', 'threshold', and
 * 'digest').
 * @param  string $delivery_type
 *   The delivery type: 'immediate', 'interval', 'threshold', or 'digest'.
 * @return array
 *   An array of entity IDs for the watchlist entries that match the requested
 *   delivery type.
 */
function watchdog_notifier_return_watchlist($delivery_type = NULL) {
  if (empty($delivery_type)) {
    return NULL;
  }
  $query = new EntityFieldQuery();
  $query->entityCondition('entity_type', 'watchdog_notifier')
    ->entityCondition('bundle', 'watchdog_notifier_bundle')
    ->propertyCondition('delivery_method', $delivery_type, '=');
  $result = $query->execute();
  $wn_ids = array();
  if (array_key_exists('watchdog_notifier', $result)) {
    foreach ($result['watchdog_notifier'] as $res) {
      $wn_ids[] = $res->wn_id;
    }
  }
  return $wn_ids;
}
