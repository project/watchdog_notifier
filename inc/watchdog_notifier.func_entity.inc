<?php

/**
 * @file
 * Entity func code for the Watchdog Notifier module.
 */


/**
 * Fetch a basic object.
 *
 * This function ends up being a shim between the menu system and
 * watchdog_notifier_load_multiple().
 *
 * This function gets its name from the menu system's wildcard naming
 * conventions. For example, /path/%wildcard would end up calling
 * wildcard_load(%wildcard value). In our case defining the path:
 * admin/config/system/watchdog_notifier/view/%watchdog_notifier in hook_menu()
 * tells Drupal to call watchdog_notifier_load().
 *
 * @param int wn_id
 *   Integer specifying the basic entity id.
 * @param bool $reset
 *   A boolean indicating that the internal cache should be reset.
 *
 * @return object
 *   A fully-loaded $basic object or FALSE if it cannot be loaded.
 *
 * @see watchdog_notifier_load_multiple()
 * @see watchdog_notifier_menu()
 */
function watchdog_notifier_load($wn_id = NULL, $reset = FALSE) {
  $wn_ids = (isset($wn_id) ? array($wn_id) : array());
  $wn = watchdog_notifier_load_multiple($wn_ids, array(), $reset);
  return $wn ? reset($wn) : FALSE;
}


/**
 * Loads multiple basic entities.
 *
 * We only need to pass this request along to entity_load(), which will in turn
 * call the load() method of our entity controller class.
 */
function watchdog_notifier_load_multiple($wn_ids = FALSE, $conditions = array(), $reset = FALSE) {
  return entity_load('watchdog_notifier', $wn_ids, $conditions, $reset);
}


/**
 * We save the entity by calling the controller.
 */
function watchdog_notifier_save(&$entity) {
  return entity_get_controller('watchdog_notifier')->save($entity);
}


/**
 * Use the controller to delete the entity.
 */
function watchdog_notifier_delete($entity) {
  return entity_get_controller('watchdog_notifier')->delete($entity);
}

