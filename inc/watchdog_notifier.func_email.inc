<?php
/**
 * @file
 * Email generation include for the Watchdog Notifier module.
 *
 * Functions for generating notifier emails are in this file.
 */

function watchdog_notifier_create_email_title($notifier = NULL) {
  if (empty($notifier)) {
    return NULL;
  }

  return t('[Watchdog Notifier] @server: \'@type\' (@severity) {@delivery}', array(
    '@type'     => $notifier->type,
    '@severity' => watchdog_notifier_get_severity($notifier->severity),
    '@server'   => gethostname(),
    '@delivery' => $notifier->delivery_method,
  ));
}


function watchdog_notifier_create_log_url($log_entry = NULL) {
  if (empty($log_entry)) {
    return NULL;
  }
  global $base_url;

  $wid = '';
  if (is_array($log_entry)) {
    $wid = $log_entry['wid'];
  } else {
    $wid = $log_entry->wid;
  }
  return $base_url . '/admin/reports/event/' . $wid;
}


function watchdog_notifier_create_message_object($log_entry = NULL) {
  if (empty($log_entry)) {
    return NULL;
  }
  $result            = new stdClass();
  $result->type      = $log_entry['type'];
  $result->severity  = watchdog_notifier_get_severity($log_entry['severity'] + 1);
  $result->timestamp = $log_entry['timestamp'];
  $result->string    = '';

  // Message.
  $variables = $log_entry['variables'];
  if (is_string($variables)) {
    $variables = unserialize($variables);
  }
  if (is_array($variables)) {
    $result->string = strtr($log_entry['message'], json_decode(json_encode($variables), true));
  }

  // Possible empty values.
  if (!empty($log_entry['link'])) {
    $result->link = $log_entry['link'];
  }
  if (!empty($log_entry['referer'])) {
    $result->referer = $log_entry['referer'];
  }

  // Username.
  if ($account = user_load($log_entry['uid'])) {
    $result->username = $account->name;
  } else {
    $result->username = dt('Anonymous');
  }

  $result->string = truncate_utf8(strip_tags(decode_entities($result->string)), PHP_INT_MAX, FALSE, FALSE);

  return $result;
}


function watchdog_notifier_create_url_list($unserialized_severities = null, $log_entries = NULL) {
  if (empty($log_entries)) {
    return NULL;
  }

  $return_msg = '';

  $severities = unserialize($unserialized_severities);
  foreach ($severities as $key => $severity) {
    if ((is_string($severity)) && ($key == intval($severity))) {
      $return_msg .= 'SEVERITY: ' . strtoupper(watchdog_notifier_get_severity($severity)) . "\n";

      foreach ($log_entries as $key => $log) {
        $log_severity = $log->severity + 1;
        if ($log_severity == $severity) {
          $return_msg .= '* ' . format_date($log->timestamp) . "\n  " . watchdog_notifier_create_log_url($log) . "\n  ";
          if (!empty($log->message)) {
            if ((!empty($log->variables)) && ($log->variables != 'N;')) {
              $output = t($log->message, unserialize($log->variables));
              $return_msg .= truncate_utf8(filter_xss($output, array()), 64, TRUE, TRUE);
            } else {
              $return_msg .= $log->message;
            }
          }
          $return_msg .= "\n";
        }
      }
      $return_msg .= "\n";
    }
  }

  return $return_msg;
}


/**
 * Implements hook_mail().
 */
function watchdog_notifier_mail($key, &$message, $params) {
  switch ($key) {
    case 'information':
      $message['subject'] = $params['subject'];
      $message['body']    = $params['body'];
      break;
    }
}


function watchdog_notifier_parse_digest_log($notifier = NULL, $log_entries = NULL) {
  $generate_digest = variable_get('watchdog_notifier_generate_digest');
  $timing_string = '';
  if ($generate_digest >= 1) {
    $timing_string = $generate_digest . ' hour(s)';
  } else {
    $timing_string = ($generate_digest * 60) . ' minute(s)';
  }

  $msg = t('This watchdog log digest is generated every @timing.', array('@timing' => $timing_string));
  $msg .= "\n\n";

  if (empty($log_entries)) {
    $msg .= t('There are no matching watchdog log messages to report.');
  } else {
    $msg .= watchdog_notifier_create_url_list($notifier->severity, $log_entries);
  }
  return array($msg);
}


function watchdog_notifier_parse_individual_log($log_entry = NULL) {
  if (is_object($log_entry)) {
    $log_entry = (array) $log_entry;
  }

  $msg    = '';
  $result = watchdog_notifier_create_message_object($log_entry);

  if (array_key_exists('wid', $log_entry)) {
    $msg .= '* Watchdog log message url: ' . watchdog_notifier_create_log_url($log_entry) . "\n\n";
  } else {
    $msg .= "* No message URL is available because this notification was sent immediately upon logging.\n\n";
  }

  $msg .= '* Watchdog log message timestamp: ' . format_date($result->timestamp) . "\n\n";
  $msg .= "* Watchdog log message contents:\n\n" . $result->string . "\n\n";

  $tmp_result = clone $result;
  unset($tmp_result->body_message);
  $msg .= "* Full message object:\n\n" . print_r($tmp_result, TRUE) . "\n";

  return array($msg);
}


function watchdog_notifier_parse_threshold_log($notifier = NULL, $log_entries = NULL) {
  $msg = t('The following @num \'@type\' watchdog log messages have exceeded a
    configured threshold (@limit messages in @time minutes).', array(
      '@num'   => count($log_entries),
      '@type'  => $notifier->type,
      '@limit' => $notifier->threshold_messages_limit,
      '@time'  => $notifier->threshold_time_range,
  ));
  $msg .= "\n\n";
  $msg .= watchdog_notifier_create_url_list($notifier->severity, $log_entries);

  return array($msg);
}


/**
 * [watchdog_notifier_send_email description]
 * @param  [type] $notifier    [description]
 * @param  [type] $log_entries [description]
 * @return [type]              [description]
 */
function watchdog_notifier_send_email($notifier = NULL, $log_entries = NULL) {
  if (empty($notifier)) {
    return FALSE;
  }

  $email_params            = array();
  $email_params['subject'] = watchdog_notifier_create_email_title($notifier);

  if (($notifier->delivery_method == 'immediate') || ($notifier->delivery_method == 'interval')) {
    if (!empty($log_entries)) {
      $email_params['body'] = watchdog_notifier_parse_individual_log($log_entries);
    }
  } elseif ($notifier->delivery_method == 'threshold') {
    $email_params['body'] = watchdog_notifier_parse_threshold_log($notifier, $log_entries);
  } elseif ($notifier->delivery_method == 'digest') {
    $email_params['body'] = watchdog_notifier_parse_digest_log($notifier, $log_entries);
  }

  // This is what references watchdog_notifier_mail().
  return drupal_mail(
    'watchdog_notifier',
    'information',
    $notifier->email,
    language_default(),
    $email_params,
    variable_get('site_mail', $notifier->email)
  );
}

